﻿using IdentityServer4;
using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    internal class Clients
    {
        public static IEnumerable<Client> Get()
        {
            return new List<Client>
        {
            new Client
            {
                ClientId = "oauthClient",
                ClientName = "Example client application using client credentials",
                AllowedGrantTypes = new List<string> { "authorization_code" },
                ClientSecrets = new List<Secret> {new Secret("SuperSecretPassword".Sha256())},
                AllowedScopes = new List<string> {"public"},
                RedirectUris =new List<String> { "https://localhost:8000/" },
                

            },
            new Client
            {
                ClientId = "adminPanel",
                ClientName = "Klient - admin panel",
                AllowedGrantTypes = new List<string> { "authorization_code" },
                RequirePkce = true,
                RequireClientSecret = false,
                AccessTokenType = AccessTokenType.Reference,
                RefreshTokenUsage=TokenUsage.OneTimeOnly,
                ClientSecrets = new List<Secret> {new Secret("SuperSecretPassword".Sha256())},
                AllowedScopes = new List<string> { "public", IdentityServerConstants.StandardScopes.OfflineAccess},
                RedirectUris =new List<String> { "https://localhost:4200/auth-callback" },
                AllowedCorsOrigins=new List<String>{ "https://localhost:4200" },
                AllowAccessTokensViaBrowser = true,
                AllowOfflineAccess=true,
                AlwaysIncludeUserClaimsInIdToken=true,


            },
            new Client
            {
                ClientId = "skoruba_identity_admin",
                ClientName = "Example client application using client credentials",
                AllowedGrantTypes = new List<string> { "authorization_code" },
                ClientSecrets = new List<Secret> {new Secret("skoruba_admin_client_secret".Sha256())},
                AllowedScopes = new List<string> {"openid","profile","email","roles" },
                RedirectUris =new List<String> { "https://localhost:44303/" },


            }
        };
        }
    }
}
