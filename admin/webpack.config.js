//const { mode } = require("webpack-nano/argv");
const { merge } = require("webpack-merge");
//const parts = require("./webpack.parts");

const commonConfig = require('./webpack.common');
const productionConfig = require('./webpack.prod');
const developmentConfig = require('./webpack.dev');

/*
const commonConfig = merge([
  { entry: ["./src"] },
  parts.page({ title: "Demo" }),
]);

const productionConfig = merge([]);

const developmentConfig = merge([
  { entry: ["webpack-plugin-serve/client"] },
  parts.devServer(),
]);
*/

const getConfig = (mode) => {
  switch (mode) {
    case "production":
      return merge(commonConfig, productionConfig);
    case "development":
      return merge(commonConfig, developmentConfig);
    default:
      throw new Error(`Trying to use an unknown mode, ${mode}`);
  }
};

module.exports = (env) =>{
  return getConfig(env.mode);
}
