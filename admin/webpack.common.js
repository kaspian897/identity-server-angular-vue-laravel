const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
//const Dotenv = require('dotenv-webpack');
var dotenv = require('dotenv').config({
  path: './.env'
});
module.exports = {

  entry: './src/index',

  output: {

    path: path.join(__dirname, '/build'),

    filename: 'bundle.js'

  },

  resolve: {

    extensions: ['.ts', '.tsx', '.js', '.jsx']

  },

  module: {

    rules: [{
        test: /\.bundle\.js$/,

        use: {

          loader: 'bundle-loader',

          options: {

            name: 'my-chunk',

            cacheDirectory: true,

            presets: ['@babel/preset-env']

          }

        }

      },

      {

        test: /\.(js|jsx)$/,

        loader: 'babel-loader',

        exclude: '/node_modules/'

      },

      {

        test: /\.(ts|tsx)$/,

        exclude: /node_modules/,

        use: ['babel-loader', "ts-loader"],

      },

      {

        test: /\.s[ac]ss$/,

        use: ['style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            }
          }
        ]

      },

      {

        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader']

      },

      {

        test: /\.jpe?g|png$/,

        exclude: /node_modules/,

        use: ["url-loader", "file-loader"]

      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|svg)$/i,
        type: 'asset/resource',
      }

    ]

  },

  plugins: [

    //new Dotenv({
    //  path: './.env',
    //}),
    new webpack.DefinePlugin({
      "process.env": JSON.stringify(process.env),
    }),
    new HtmlWebpackPlugin({

      template: './src/index.html',
      templateParameters: {
        title: process.env.SITE_TITLE
      },


    }),

  ]
};