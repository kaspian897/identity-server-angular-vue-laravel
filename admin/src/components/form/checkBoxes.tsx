import React from "react";
import { Field, ErrorMessage,FieldProps  } from "formik";
import { Option } from "./select";
export interface ICheckboxesProps {
  label: string;
  name: string;
  options: Array<Option>;
}

export interface ICheckboxesState {}

export default class Checkboxes extends React.Component<
  ICheckboxesProps,
  ICheckboxesState
> {
  constructor(props: ICheckboxesProps) {
    super(props);

    this.state = {};
  }

  public render() {
    const { label, name, options, ...rest } = this.props;
    return (
      <div>
        <label>{label}</label>
        <Field name={name}>
          {(formik:FieldProps) => {
            const { field } = formik;
            return options.map((option) => {
              return (
                <div key={option.key}>
                  <input
                    type="checkbox"
                    id={option.value}
                    {...field}
                    {...rest}
                    value={option.value}
                    checked={field.value.includes(option.value)}
                  />
                  <label>{option.key}</label>
                </div>
              );
            });
          }}
        </Field>
        <ErrorMessage name={name} />
      </div>
    );
  }
}
