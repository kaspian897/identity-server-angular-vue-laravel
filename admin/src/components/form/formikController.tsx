import React from "react";
import Input, { IInputProps } from "./input";
import TextArea, { ITextAreaProps } from "./textArea";
import Select, { ISelectProps } from "./select";
import RadioButtons from "./radioButton";
import CheckBoxes from "./checkBoxes";

export interface IFormikControllerProps {
  typeName: string;
  [x:string]: any;
}

export interface IFormikControllerState {}

type AllType = IFormikControllerProps &
  (IInputProps | ITextAreaProps | ISelectProps) ;

// function isOfType<T>(
//   varToBeChecked: any,
//   propertyToCheckFor: keyof T
// ): varToBeChecked is T {
//   return (varToBeChecked as T)[propertyToCheckFor] !== undefined;
// }

export default class FormikController extends React.Component<
  AllType,
  IFormikControllerState
> {
  constructor(props: AllType) {
    super(props);
    this.state = {};
  }

  public render() {
    const isOfType = <T extends {}, K>(
      varToBeChecked: any,
      propertyToCheckFor: keyof T,
      value: T[keyof T]
    ): varToBeChecked is K =>
      (varToBeChecked as T)[propertyToCheckFor] === value;
    // const isString = (variableToCheck: any): variableToCheck is string =>
    //   (variableToCheck as string).toLowerCase !== undefined;

    const { typeName, ...rest } = this.props;
    // if (typeof type === "IInputProps") {
    // }
    // isString(this);
    if (isOfType<AllType, IInputProps>(this.props, "typeName", "input"))
      return <Input {...this.props} />;
    if (isOfType<AllType, ISelectProps>(this.props, "typeName", "select"))
      return <Select {...this.props} />;
    // if (isOfType<AllType, ISelectProps>(this.props, "typeName", "select"))
    //   return <Select {...this.props} />;
    // switch (typeName) {
    //   case "input":
    //     return <Input {...rest} />;
    //   case "textArea":
    //     return <TextArea {...rest} />;
    //   case "select":
    //     if (isOfType<AllType, ISelectProps>(this.props, "typeName", "select"))
    //       return <Select {...this.props} />;
    //     return null;
    //   // case "radio":
    //   //   return <RadioButtons {...this.props.tProps} />;
    //   // case "checkbox":
    //   //   return <CheckBoxes {...this.props.tProps} />;
    //   default:
    return null;
  }
}
