import React from "react";
import { Field, ErrorMessage,FieldProps } from "formik";
import {Option} from "./select"
export interface IRadioButtonsProps {
  label:string;
  name:string;
  options:Array<Option>
}

export interface IRadioButtonsState {
}

export default class RadioButtons extends React.Component<IRadioButtonsProps, IRadioButtonsState> {
  constructor(props: IRadioButtonsProps) {
    super(props);

    this.state = {
    }
  }

  public render() {
    const { label, name, options, ...rest } = this.props;
    return (
      <div>
        <label>{label}</label>
        <Field name={name}>
          {(formik:FieldProps) => {
            const { field } = formik;
            return options.map((option) => {
              return (
                <div key={option.key}>
                  <input
                    type="radio"
                    id={option.value}
                    
                    {...field}
                    {...rest}
                    value={option.value}
                    checked={field.value === option.value}
                  />
                  <label htmlFor={option.value}>{option.key}</label>
                </div>
              );
            });
          }}
        </Field>
        <ErrorMessage name={name} />
      </div>
    );
  }
}

