import React from "react";
import { Field, ErrorMessage } from "formik";

export interface ITextAreaProps {
  name: string;
  label: string;
}

export interface ITextAreaState {
}

export default class TextArea extends React.Component<ITextAreaProps, ITextAreaState> {
  constructor(props: ITextAreaProps) {
    super(props);

    this.state = {
    }
  }

  public render() {
    const { label, name, ...rest } = this.props;
    return (
      <div>
        <label htmlFor={name}>{label}</label>
        <Field as="textarea" id={name} name={name} {...rest} />
        <ErrorMessage name={name} />
      </div>
    );
  }
}


