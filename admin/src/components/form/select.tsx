import React, { OptionHTMLAttributes } from "react";
import { Field, ErrorMessage } from "formik";

export type Option = {
    key: string;
    value: string;
  };
export interface ISelectProps{
    label: string;
    name: string;
    options: Array<Option>;

  }
export interface ISelectState {
}

export default class Select extends React.Component<ISelectProps, ISelectState> {
  constructor(props: ISelectProps) {
    super(props);

    this.state = {
    }
  }

  public render() {
    const { label, name, options, ...rest } = this.props;
  return (
    <div>
      <label htmlFor={name}>{label}</label>
      <Field as="select" id={name} name={name} {...rest}>
        {options.map((option) => {
          return (
            <option key={option.key} value={option.value}>
              {option.key}
            </option>
          );
        })}
      </Field>
      <ErrorMessage name={name} />
    </div>
  );
  }
}


