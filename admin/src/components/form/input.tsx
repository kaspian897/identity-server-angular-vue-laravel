import React from "react"
import { Field, ErrorMessage } from "formik"


  
  export interface IInputProps {
    name: string;
    label: string;
  }
  
  export interface IInputState {
  }
  
  export default class Input extends React.Component<IInputProps, IInputState> {
    constructor(props: IInputProps) {
      super(props);
  
      this.state = {
        
      }
    }
  
    public render() {
      const { name, label, ...rest } = this.props;
      return (
        <div>
          <label htmlFor={name}> {label}</label>
          <Field name={name} {...rest} />
          <ErrorMessage name={name} />
        </div>
      );
    }
  }
  
