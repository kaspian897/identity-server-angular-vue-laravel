import * as React from "react";
import "./avatar.scss";
export interface IAvatarProps {}

export interface IAvatarState {}

export default class Avatar extends React.Component<
    IAvatarProps,
    IAvatarState
> {
    constructor(props: IAvatarProps) {
        super(props);

        this.state = {};
    }

    public render() {
        return  <img src="https://www.w3schools.com/w3images/avatar5.png" alt="Avatar" className="avatar"/> ;
    }
}
