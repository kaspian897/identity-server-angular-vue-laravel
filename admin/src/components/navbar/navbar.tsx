import * as React from "react";
import { Link } from "react-router-dom";
import  Logo  from "./logo";
import { AiOutlineMenu } from "react-icons/ai";
import Avatar from "./avatar";
import "./navbar.scss";
export interface INavbarProps {
  onClick?: React.MouseEventHandler;
}

export interface INavbarState {}

export default class Navbar extends React.Component<
  INavbarProps,
  INavbarState
> {
  constructor(props: INavbarProps) {
    super(props);
    this.state = {};
  }
  public render() {
    return (
      <nav className="nav-navbar">
        <AiOutlineMenu
            className="menu-button"
            onClick={this.props.onClick}
          />
        <Logo/>
        <div className="search-bar">
          search
        </div>
        
        <Avatar/>
      </nav>
    );
  }
}
