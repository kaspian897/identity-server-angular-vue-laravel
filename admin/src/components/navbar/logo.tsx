import * as React from "react";
import { FiLogOut } from "react-icons/fi";
import "./logo.scss";
export interface ILogoProps {}

export interface ILogoState {}

export default class Logo extends React.Component<ILogoProps, ILogoState> {
    constructor(props: ILogoProps) {
        super(props);

        this.state = {};
    }

    public render() {
        return (
            <div className="logo">
                <FiLogOut className="logo-icon" />
                Logo
            </div>
        );
    }
}
