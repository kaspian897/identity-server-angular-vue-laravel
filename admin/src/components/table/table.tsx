import * as React from "react";
import "./table.scss";

export interface IColumnDefinitionType<T, K extends keyof T> {
    key: K;
    header: string;
}

export interface ITableProps<T, K extends keyof T> {
    data: Array<T>;
    columns: Array<IColumnDefinitionType<T, K>>;
}

export interface ITableState {}

export default class Table<T, K extends keyof T> extends React.Component<
    ITableProps<T, K>,
    ITableState
> {
    constructor(props: ITableProps<T, K>) {
        super(props);

        this.state = {};
    }

    public render() {
        const data = this.props.data;
        const columns = this.props.columns;
        const rows = data.map((row, index) => {
            return (
                <tr>
                    {columns.map((column, index2) => {
                        return <td>{row[column.key]}</td>;
                    })}
                </tr>
            );
        });
        const headers = columns.map((column, index) => {
            return <th>{column.header}</th>;
        });
        return (
            <table>
                <thead>
                    <tr>{headers}</tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
}
