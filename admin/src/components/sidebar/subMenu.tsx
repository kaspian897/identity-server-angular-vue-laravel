import * as React from "react";
import { BsFilePost } from "react-icons/bs";
import { IoIosArrowUp, IoIosArrowDown } from "react-icons/io";
import { Link } from "react-router-dom";
export interface ISubMenuProps {
  children?: React.ReactNode;
}

export interface ISubMenuState {
  open: boolean;
}

export default class SubMenu extends React.Component<
  ISubMenuProps,
  ISubMenuState
> {
  constructor(props: ISubMenuProps) {
    super(props);

    this.state = {
      open: false,
    };
    this.toggleMenuLevel = this.toggleMenuLevel.bind(this);
  }
  toggleMenuLevel() {
    this.setState({ open: !this.state.open });
  }

  public render() {
    return (
      <>
        <span  onClick={this.toggleMenuLevel }>
          <BsFilePost className="icon" />
          <span>Blog</span>
          {this.state.open ? (
            <IoIosArrowUp  className="arrow" />
          ) : (
            <IoIosArrowDown className="arrow" />
          )}
        </span>
        <ul className={this.state.open ? "" : "hidden-level"}>
          {this.props.children}
        </ul>
      </>
    );
  }
}
