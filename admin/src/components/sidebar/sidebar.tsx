import React, { useState } from "react";
import { Link } from "react-router-dom";
import { BsFilePost } from "react-icons/bs";
import { HiOutlineUserGroup } from "react-icons/hi";
import { RiDashboardLine } from "react-icons/ri";
import { AiOutlineMenu } from "react-icons/ai";
import { FiLogOut } from "react-icons/fi";
import { IoIosArrowUp, IoIosArrowDown } from "react-icons/io";
import { TiDocumentAdd } from "react-icons/ti";
import SubMenu from "./subMenu";
import Footer from "./footer";
import "./sidebar.scss";

export interface ISidebarProps {}

export interface ISidebarState {
  sidebar: boolean;
}

export default class Sidebar extends React.Component<
  ISidebarProps,
  ISidebarState
> {
  constructor(props: ISidebarProps) {
    super(props);

    this.state = {
      sidebar: true,
    };

    this.toggleSidebar = this.toggleSidebar.bind(this);
  }

  toggleSidebar() {
    this.setState({ sidebar: !this.state.sidebar });
  }

  public render() {
    return (
      <>
        <nav
          className={this.state.sidebar ? "nav-sidebar active" : "nav-sidebar"}
        >
          <ul className="nav-menu-items">
            <li>
              <SubMenu>
                  <li>
                    <Link to="/">
                      <TiDocumentAdd className="icon" />
                      <span>Blog</span>
                    </Link>
                  </li>
                  <li>
                    <Link to="/">
                      <BsFilePost className="icon" />
                      <span>Blog</span>
                    </Link>
                  </li>
                
              </SubMenu>
            </li>
            <li>
              <Link to="/about">
                <HiOutlineUserGroup className="icon" />
                <span>Użytkownicy</span>
              </Link>
            </li>
            <li>
              <Link to="/topics">
                <RiDashboardLine className="icon" />
                <span>Panel główny</span>
              </Link>
            </li>
          </ul>
          <Footer/>
        </nav>
        
      </>
    );
  }
}
