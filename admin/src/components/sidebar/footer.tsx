import * as React from "react";
import { IoHeart } from "react-icons/io5";
export interface IFooter {}
export default function Footer(props: IFooter) {
  return (
    <footer className="footer">
      <h4>
        Made with <IoHeart className="heart" />
      </h4>
    </footer>
  );
}
