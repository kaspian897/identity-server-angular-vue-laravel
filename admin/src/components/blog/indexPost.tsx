import * as React from "react";
import { IPost } from "./types";
import Table, { IColumnDefinitionType } from "../table/table";

export interface IIndexPostProps {}

export interface IIndexPostState {
    error: Error | null;
    isLoaded: boolean;
    items: Array<IPost>;
}

export default class IndexPost extends React.Component<
    IIndexPostProps,
    IIndexPostState
> {
    constructor(props: IIndexPostProps) {
        super(props);

        this.state = {
            error: null,
            isLoaded: false,
            items: [],
        };
    }
    componentDidMount() {
        fetch("http://127.0.0.1:8000/api/blog")
            .then((res) => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.data,
                    });
                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error,
                    });
                }
            );
    }
    public render() {
        const columns: IColumnDefinitionType<IPost, keyof IPost>[] = [
            {
                key: "id",
                header: "Name",
            },
            {
                key: "title",
                header: "Age in years",
            },
            {
                key: "description",
                header: "Age in years",
            }
        ];
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Błąd: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Ładowanie...</div>;
        } else {
            return <Table data={items} columns={columns} />;
        }
    }
}
