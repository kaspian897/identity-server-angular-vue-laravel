import React, { useState, useRef } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Editor } from "@tinymce/tinymce-react";
import FormikController from "../form/formikController";
import "./blog.scss";
import "../form/form.scss";

//import "../../_grid.scss";
export interface IAddPostProps {}

export interface IAddPostState {
  value: string;
}

export default class AddPost extends React.Component<
  IAddPostProps,
  IAddPostState
> {
  constructor(props: IAddPostProps) {
    super(props);

    this.state = {
      value: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event: React.ChangeEvent<HTMLTextAreaElement>) {
    this.setState({ value: event.target.value });
  }
  handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    alert("Wysłano następujące wypracowanie: " + this.state.value);
    event.preventDefault();
  }

  public render() {
    const choices = [
      { key: "choice a", value: "choicea" },
      { key: "choice b", value: "choiceb" },
    ];

    const initialValues = {
      title: "",
      description: "",
      selectChoice: "",
      radioChoice: "",
      checkBoxChoice: "",
    };
    const validationSchema = Yup.object({
      title: Yup.string().required("Required"),
      description: Yup.string().required("Required"),
      selectChoice: Yup.string().required("Required"),
      radioChoice: Yup.string().required("Required"),
      checkBoxChoice: Yup.array().required("Required"),
    });

    return (
      <div className="flex-container">
        <div className="box"></div>
        <div className="row">
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values, actions) => {
              console.log({ values, actions });

              alert(JSON.stringify(values, null, 2));

              actions.setSubmitting(false);
            }}
          >
            {(formik) => (
              <div className="col-9 col-xs-1">
                <Form>
                  <div className="row">
                    <div className="col-12">
                      <label className="input-label" htmlFor="title">
                        Tytuł
                      </label>
                      <Field
                        className="input"
                        id="title"
                        name="title"
                        placeholder="Tytuł"
                      />
                      <ErrorMessage name="title" />
                      <FormikController typeName="input" label="aaa" name="bbb" type="email"/>
                    </div>
                    <div className="col-6">
                      <label className="input-label" htmlFor="title">
                        Tytuł
                      </label>
                      <Field
                        className="input"
                        id="title"
                        name="title"
                        placeholder="Tytuł"
                      />
                      <ErrorMessage name="title" />
                    </div>
                  </div>
                  <Field type="email" name="email" />
                  <Editor
                    initialValue="<p>This is the initial content of the editor.</p>"
                    init={{
                      height: 500,
                      menubar: false,
                      plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table paste code help wordcount",
                      ],
                      toolbar:
                        "undo redo | formatselect | " +
                        "bold italic backcolor | alignleft aligncenter " +
                        "alignright alignjustify | bullist numlist outdent indent | " +
                        "removeformat | help",
                      content_style:
                        "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                    }}
                  />
                  <button type="submit" className="button-yellow">
                    Submit
                  </button>
                </Form>
              </div>
            )}
          </Formik>
        </div>
      </div>
    );
  }
  /*public render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Wypracowanie:
          <textarea
            value={this.state.value}
            onChange={this.handleChange}
          />{" "}
        </label>
        <input type="submit" value="Wyślij" />
      </form>
    );
  }*/
}
