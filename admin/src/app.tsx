import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from "react-router-dom";

import "./app.scss";
import Sidebar from "./components/sidebar/sidebar";
import Navbar from "./components/navbar/navbar";
import AddPost from "./components/blog/addPost";
import IndexPost from "./components/blog/indexPost";
import Test from "./components/router";

export interface IAppProps {}

export interface IAppState {}

export default class App extends React.Component<IAppProps, IAppState> {
  private sidebar: React.RefObject<Sidebar>;
  constructor(props: IAppProps) {
    super(props);

    this.sidebar = React.createRef();
    this.state = {};
    this.toggleSidebar=this.toggleSidebar.bind(this);
  }
  public toggleSidebar(){
    if(this.sidebar.current) 
      this.sidebar.current.toggleSidebar();
  }
  public render() {
    return (
      <>
        <Router>
          <Navbar onClick={this.toggleSidebar} />
          <div className="body-container">
            <Sidebar ref={this.sidebar} />
            <div className="board-container">
              <Switch>
                <Route path="/about">
                  <AddPost />
                </Route>
                <Route path="/topics">
                  <IndexPost />
                </Route>
              </Switch>
            </div>
          </div>
        </Router>
      </>
    );
  }
}
